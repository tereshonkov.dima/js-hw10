"use strict"
//1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.

const newA = document.createElement("a");
const footer = document.getElementsByTagName("footer")[0]; //Так как нам возваращет колекцию то нам нужен ее первый элемент [0]
newA.href = "#";
footer.append(newA);

// 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".

const select = document.createElement("select");
select.id = "rating";
const main = document.getElementsByTagName("main")[0];
main.prepend(select);

// // Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.

// const option4 = document.createElement("option");
// option4.value = 4;
// option4.textContent = "4 Stars";
// select.append(option4);

// // Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.

// const option3 = document.createElement("option");
// option3.value = 3;
// option3.textContent = "3 Stars";
// select.prepend(option3);

// // Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.

// const option2 = document.createElement("option");
// option2.value = 2;
// option2.textContent = "2 Stars";
// select.prepend(option2);

// // Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

// const option1 = document.createElement("option");
// option1.value = 1;
// option1.textContent = "1 Stars";
// select.prepend(option1);



//Исправление ошибок, выполнение задание через цикл for
for (let i = 1; i <= 5; i++) {
    const newOption = document.createElement("option");
    newOption.value = i;
    newOption.textContent = i + `Star`;
    select.append(newOption);
};


